import React, {Component} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  View,
  FlatList,
  Text,
  ActivityIndicator,
} from 'react-native';
import {Button} from './Button';
import SQLite from 'react-native-sqlite-storage';
import TaskItem from './TaskItem';
import NoTask from './NoTask';
import Loading from './Loading';
import {dbServices, executeQuery} from './DbServices';
import axios from 'axios';
import {httpRequest} from './HttpRequest';
const API_URL = 'https://task-manager-api-nima.herokuapp.com/api';

export default class Home extends Component {
  constructor(props) {
    super(props);

    this.db = dbServices;

    this.state = {
      taskList: [],
      loading: false,
    };

    //  this.getInitiatingData();
    // console.log(taskList);
  }

  async getInitiatingData() {
    const respond = await this.getAllTasks();
    console.log(respond);

    const {data} = respond;
    this.setState({
      taskList: data,
    });
  }

  async componentDidMount() {
    let results = await executeQuery('SELECT * FROM task ORDER BY id DESC', []);
    const rows = results.rows;
    let taskList = [];
    for (let i = 0; i < rows.length; i++) {
      console.log(rows.item(i), 'task componentDidMount');
      taskList.push({
        ...rows.item(i),
      });
    }
    this.setState({
      taskList,
      loading: false,
    });
  }

  getAllTasks() {
    return httpRequest('get', `${API_URL}/allTasks`);
  }

  async createTask(data) {
    const a = await httpRequest('post', `${API_URL}/tasks`, data);
    console.log(a, 'aaaaaaa');
  }

  updateQuery(id, data) {
    axios({
      method: 'put',
      url: `${API_URL}/drivers/${id}`,
      data,
    })
      .then(function (response) {
        console.log(response);
      })
      .catch((err) => console.log(err));
  }

  deleteQuery(id) {
    axios({
      method: 'delete',
      url: `${API_URL}/drivers/${id}`,
    })
      .then(function (response) {
        console.log(response);
      })
      .catch((err) => console.log(err));
  }

  insertQuery = async (query) => {
    return await executeQuery(query, []);
  };

  onTaskItem = (item) => {
    this.props.navigation.navigate('Task', {
      item,
      saveTask: this.updateTask,
    });
  };

  onAddTask = () => {
    this.props.navigation.navigate('Task', {
      saveTask: this.addTask,
    });
  };

  addTask = (item) => {
    this.setState(
      (prevState) => ({
        taskList: [item].concat(prevState.taskList),
      }),
      () => {
        const {title, details, status, date, id} = item;
        this.createTask(item);

        let query =
          'INSERT INTO task (id, date, title, details, status ) VALUES';
        query =
          query +
          "('" +
          id + //date
          "','" +
          date + //date
          "','" +
          title + //title
          "','" +
          details + //details
          "','" +
          status + //status
          "')";
        this.insertQuery(query);
      },
    );
  };

  updateTask = (item) => {
    this.setState(
      {
        taskList: this.state.taskList.map((el) =>
          el.id === item.id ? Object.assign({}, el, item) : el,
        ),
      },
      () => {
        executeQuery(
          'UPDATE task SET title = ? , details = ? , status = ?  WHERE id = ?',
          [`${item.title}`, `${item.details}`, `${item.status}`, item.id],
        );
        alert('Task updated successfully');
      },
    );
  };

  deleteTask = (item) => {
    this.setState(
      {
        taskList: this.state.taskList.filter((el) => el.id !== item.id),
      },
      () => {
        executeQuery(`DELETE FROM task WHERE id = ${item.id};`);
      },
    );
  };

  render() {
    return (
      <SafeAreaView style={styles.container}>
        {this.state.loading ? (
          <Loading />
        ) : this.state.taskList.length > 0 ? (
          <FlatList
            style={styles.listContainer}
            data={this.state.taskList}
            renderItem={({item, index, separators}) => (
              <TaskItem
                item={item}
                index={index}
                title={item.title}
                status={item.status}
                onTaskItem={this.onTaskItem}
                onChangeStatus={this.updateTask}
                onDeleteTask={this.deleteTask}
              />
            )}
            keyExtractor={(item) => item.id}
          />
        ) : (
          <NoTask />
        )}
        <Button
          style={styles.addButton}
          onPress={this.onAddTask}
          title="Add Task"
          color="transparent"
        />
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F8F8F8',
  },
  item: {
    padding: 10,
    marginVertical: 8,
    marginHorizontal: 16,
    flex: 1,
    flexDirection: 'row',
  },
  addButton: {
    height: 60,
    justifyContent: 'center',
    marginVertical: 10,
  },
  listContainer: {flex: 1, marginTop: 40},
});
