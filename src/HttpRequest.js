import axios from 'axios';

const httpRequest = (method: String ='GET', url: any, data = null) => {
  return axios({
    method,
    url,
    data,
  });
};

module.exports = {httpRequest};
