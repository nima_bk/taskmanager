import React, {Component} from 'react';
import {
  View,
  Text,
  TextInput,
  SafeAreaView,
  StyleSheet,
  StatusBar,
  TouchableWithoutFeedback,
  Keyboard,
  YellowBox,
} from 'react-native';
import {Button} from './Button';
import DatePicker from 'react-native-datepicker';

YellowBox.ignoreWarnings([
  'Non-serializable values were found in the navigation state',
]);

const DismissKeyboard = ({children}) => (
  <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
    {children}
  </TouchableWithoutFeedback>
);

export default class Task extends Component {
  constructor(props) {
    super(props);
    this.taskParams = this.props.route.params;

    this.state = {
      title: this.taskParams.item ? this.taskParams.item.title : '',
      details: this.taskParams.item ? this.taskParams.item.details : '',
      id: this.taskParams.item ? this.taskParams.item.id : null,
      date: this.taskParams.item
        ? this.taskParams.item.date
        : new Date().toISOString().slice(0, 10), // date format yyyy-MM-dd
    };
  }

  onChangeTaskTitle = (title) => {
    this.setState({
      title,
    });
  };

  onChangeTaskDetails = (details) => {
    this.setState({
      details,
    });
  };

  saveTask = () => {
    const {title, details, id, date} = this.state;

    // title is required
    if (title !== '') {
      let item;

      //  id =  null , means new task, else edit task
      if (!id) {
        item = {
          id: Date.now(),
          title,
          details,
          status: 0,
          date,
        };
      } else {
        item = {
          id,
          title,
          details,
          status: this.taskParams.item.status,
          date,
        };
      }
      this.taskParams.saveTask(item);
      this.props.navigation.goBack();
    } else {
      alert('Please enter the title');
    }
  };

  render() {
    const {
      titleContainer,
      title,
      taskInput,
      detailsContainer,
      details,
      detailsInput,
      addButton,
      dateContainer,
      date,
      dateIcon,
      dateInput,
      dateTouchBody,
    } = styles;
    return (
      <SafeAreaView style={{flex: 1}}>
        <DismissKeyboard>
          <View style={styles.container}>
            <View style={titleContainer}>
              <Text style={title}>Title</Text>
              <TextInput
                style={taskInput}
                onChangeText={(text) => this.onChangeTaskTitle(text)}
                value={this.state.title}
              />
            </View>
            <View style={dateContainer}>
              <Text style={date}>Date</Text>
              <DatePicker
                style={{flex: 9}}
                date={this.state.date}
                mode="date"
                placeholder="select date"
                format="YYYY-MM-DD"
                confirmBtnText="Confirm"
                cancelBtnText="Cancel"
                customStyles={{
                  dateIcon,
                  dateInput,
                  dateTouchBody,
                }}
                onDateChange={(date) => {
                  this.setState({date: date});
                }}
              />
            </View>
            <View style={detailsContainer}>
              <Text style={details}>Details</Text>
              <TextInput
                multiline={true}
                numberOfLines={4}
                style={detailsInput}
                onChangeText={(text) => this.onChangeTaskDetails(text)}
                value={this.state.details}
              />
            </View>
          </View>
        </DismissKeyboard>
        <Button
          style={addButton}
          onPress={this.saveTask}
          title="Save"
          color="transparent"
        />
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: StatusBar.currentHeight || 20,
    backgroundColor: '#F8F8F8',
    marginHorizontal: 16,
  },
  titleContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 20,
  },
  title: {
    flex: 2,
    fontSize: 16,
    fontWeight: '700',
    marginRight: 10,
  },
  dateContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 20,
  },
  date: {
    flex: 2,
    fontSize: 16,
    fontWeight: '700',
    marginRight: 10,
  },
  dateIcon: {
    width: 0,
    height: 0,
  },
  dateInput: {
    marginLeft: 0,
    borderWidth: 0,
  },
  dateTouchBody: {
    height: 50,
    padding: 10,
    borderColor: 'gray',
    borderWidth: 1,
    borderRadius: 8,
  },
  taskInput: {
    flex: 8,
    height: 40,
    padding: 10,
    borderColor: 'gray',
    borderWidth: 1,
    borderRadius: 8,
  },
  detailsContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  details: {
    flex: 2,
    fontSize: 16,
    fontWeight: '700',
    marginRight: 10,
  },
  detailsInput: {
    flex: 8,
    height: 240,
    padding: 10,
    borderColor: 'gray',
    borderWidth: 1,
    borderRadius: 8,
    textAlignVertical: 'top',
  },
  addButton: {
    height: 60,
    justifyContent: 'center',
    marginVertical: 10,
  },
});
