import SQLite from 'react-native-sqlite-storage';

class DbServices {
  constructor() {
    openDatabase();
  }
}

function openDatabase() {
  this.db = SQLite.openDatabase(
    {
      name: 'TaskDB.db',
      location: 'default',
      createFromLocation: '~www/TaskDB.db',
    },
    () => {},
    (error) => {
      console.log(error);
    },
  );
}

function executeQuery(sql, params = []) {
  return new Promise((resolve, reject) => {
    this.db.transaction((tx) => {
      tx.executeSql(
        sql,
        params,
        (tx, results) => {
          resolve(results);
        },
        (error) => {
          console.log(error);
        },
      );
    });
  });
}

let dbServices = new DbServices();
export {dbServices, executeQuery};
