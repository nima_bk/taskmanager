import React, {Component} from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';

export default class TaskItem extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isSelected: this.props.item.status,
    };
  }

  onTaskItem = (item) => {
    this.props.onTaskItem(item);
  };

  completeTask = (i) => {
    this.setState({isSelected: !this.state.isSelected}, () => {
      const item = i;
      item.status = this.state.isSelected ? 1 : 0;
      this.props.onChangeStatus(item);
    });
  };

  deleteTask = (i) => {
    this.props.onDeleteTask(i);
  };

  render() {
    const {item} = this.props;
    const {
      container,
      dueContainer,
      dateTxt,
      title,
      iconContainer,
      checkIcon,
      trashIcon,
    } = styles;

    return (
      <TouchableOpacity onPress={() => this.onTaskItem(item)} style={container}>
        <View
          style={{
            flex: 1,
          }}>
          <Text style={title}>{item.title}</Text>
          <View style={dueContainer}>
            <Text style={[dateTxt, {fontWeight: '500'}]}>Due Date :</Text>
            <Text style={dateTxt}> {item.date}</Text>
          </View>
        </View>
        <TouchableOpacity
          onPress={() => this.deleteTask(item)}
          activeOpacity={0.7}
          style={iconContainer}>
          <Image source={require('./images/trash.png')} style={trashIcon} />
        </TouchableOpacity>
        <TouchableOpacity
          activeOpacity={0.7}
          onPress={() => this.completeTask(item)}
          style={iconContainer}>
          {this.state.isSelected ? (
            <Image
              source={require('./images/check_circle.png')}
              style={checkIcon}
            />
          ) : (
            <Image
              source={require('./images/outline_circle.png')}
              style={checkIcon}
            />
          )}
        </TouchableOpacity>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    marginHorizontal: 16,
    height: 80,
    borderWidth: 1,
    borderColor: '#D3D3D3',
    borderRadius: 12,
    marginBottom: 16,
    paddingHorizontal: 16,
  },
  dueContainer: {flexDirection: 'row', alignItems: 'center'},
  iconContainer: {padding: 15, borderWidth: 0},
  checkIcon: {height: 35, width: 35, tintColor: '#3265EC'},
  trashIcon: {height: 25, width: 25, tintColor: 'gray'},
  title: {
    fontSize: 18,
    fontWeight: '500',
  },
  dateTxt: {
    fontSize: 14,
    marginTop: 8,
  },
});
