import React, {Component} from 'react';
import {StyleSheet, Text, View} from 'react-native';

export default class NoTask extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.text}>Please Insert New Task</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    fontSize: 32,
    fontWeight: '500',
    paddingHorizontal: '4%',
  },
});
