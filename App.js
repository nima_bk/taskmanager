import 'react-native-gesture-handler';
import {createStackNavigator} from '@react-navigation/stack';
import React from 'react';
import Home from './src/Home';
import {NavigationContainer} from '@react-navigation/native';
import Task from './src/Task';
import {StyleSheet} from 'react-native';

console.disableYellowBox = true;

function HomeScreen({route, navigation}) {
  return <Home route={route} navigation={navigation} />;
}
function TaskScreen({route, navigation}) {
  return <Task route={route} navigation={navigation} />;
}

const Stack = createStackNavigator();

const App = () => {
  const {headerStyle, headerTitleStyle} = styles;
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Home">
        <Stack.Screen
          name="Home"
          component={HomeScreen}
          options={{
            headerStyle,
            headerTitleStyle,
            headerTintColor: 'white',
            title: 'Tasks Management',
          }}
        />
        <Stack.Screen
          name="Task"
          component={TaskScreen}
          options={{
            headerStyle,
            headerTitleStyle,
            headerTintColor: 'white',
            title: 'Task Edit',
          }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default App;

const styles = StyleSheet.create({
  headerStyle: {
    backgroundColor: '#3265EC',
    height: 140,
  },
  headerTitleStyle: {
    fontWeight: 'bold',
  },
});
